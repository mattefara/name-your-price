# Name your price
Progetto universitario che permette la gestione di aste online.

## Setup

### Redis
Una volta clonata la repository e scaricati i pacchetti python è necessario installare il server redis.
Stando alla [documentazione ufficiale](https://redis.io/topics/quickstart), una volta scaricato si devono eseguire i comandi:
```shell script
# Se il software è stato già scaricato non è necessario eseguire il primo comando.
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
```

Per poter utilizzare redis anche al di fuori della cartella d'installazione:
```shell script
sudo make install
```

Per lanciare il server si digiti:
```shell script
redis-server
# Verifica di funzionamento redis
redis-cli ping 
# PONG
``` 

### Django
Lanciare il server django.
```shell script
python manage.py migrate
python manage.py runserver
```

### Celery
Ora è necessario lanciare i comandi responsabili dell'esecuzione di celery responsabile dell'esecuzione di task periodiche.
In rispettivamente due terminale lanciare.
```shell script
celery -A name_your_price worker -l info
celery -A name_your_price beat -l info
```

Se si preferisce lanciarli in unico comando eseguire:
```shell script
celery -A name-your-price worker --loglevel=info
```

## Popolamento database
Se si vuole popolare il database con dati casuali lanciare il file start.sh.
La generazione prevede l'aggiunta di aste e utenti.
Gli utenti generati sono 'user1', 'user2', 'seller1' con password 'password'.
```shell script
# Aggiungerà le migrazioni, le applicherà e genererà i dati
bash start.sh
# Per la sola generazione dei dati
python manage.py shell < random_data.py
```

Per caricare il dump del database
```shell script
python manage.py loaddata db.json
```

## Generazione uml
Se si vogliono generare gli UML dei modelli del progetto lanciare lo scipt generate_uml.sh.
Verrà generato uno schema per ogni applicazione e uno dell'intero progetto.
```shell script
bash generate_uml.sh
```

