import random

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.utils import timezone
from mixer.backend.django import mixer

from auction.models import AuctionProduct
from purchase.models import CoinBundle


def add_users():
    return [
        # Seller
        mixer.blend(User, username='seller1', password=make_password('password')),
        # Bidding user
        mixer.blend(User, username='user1', password=make_password('password')),
        mixer.blend(User, username='user2', password=make_password('password')),
    ]


def get_random_auction_data(date_func, **kwargs):
    start, end = date_func()
    data = {**kwargs}

    categories = [
        {'category': AuctionProduct.AuctionCategory.CARS, 'product_image': 'images/immagine_test_car.jpg'},
        {'category': AuctionProduct.AuctionCategory.CLOCKS, 'product_image': 'images/immagine_test_clock.jpg'},
        {'category': AuctionProduct.AuctionCategory.JEWELS, 'product_image': 'images/immagine_test_jewel.jpg'},
        {'category': AuctionProduct.AuctionCategory.TECHNOLOGY, 'product_image': 'images/immagine_test_pc.jpg'}
    ]
    data.update({
        'date_start': start,
        'date_end': end,
        **random.choice(categories)
    })
    return data


def get_current_random_dates():
    # From now to max 5 days
    start = timezone.now() + timezone.timedelta(seconds=random.choice(range(10)))
    end = start + timezone.timedelta(days=random.choice(range(1, 6)))
    return start, end


def get_future_random_dates():
    # From 1-5 days to max 5 days
    start = timezone.now() + timezone.timedelta(days=random.choice(range(1, 5)))
    end = start + timezone.timedelta(days=random.choice(range(1, 6)))
    return start, end


def get_past_random_dates():
    # From 10-5 days ago for max 3 days
    start = timezone.now() - timezone.timedelta(days=random.choice(range(5, 10)))
    end = start + timezone.timedelta(days=random.choice(range(1, 3)))
    return start, end


def add_auctions(seller):
    # From now to max 5 days
    start, end = get_current_random_dates()

    # From 1-5 days to max 5 days
    future_start = timezone.now() + timezone.timedelta(days=random.choice(range(1, 5)))
    future_end = future_start + timezone.timedelta(days=random.choice(range(1, 6)))

    # From 10-5 days ago for max 3 days
    past_start = timezone.now() - timezone.timedelta(days=random.choice(range(5, 10)))
    passed_end = past_start + timezone.timedelta(days=random.choice(range(1, 3)))

    # Pending active started auctions
    auctions = [mixer.blend(AuctionProduct, name=f'Active {i} auction',
                            **get_random_auction_data(get_current_random_dates), seller=seller, pending=True) for i in range(5)]

    # Active auction
    auctions += [mixer.blend(AuctionProduct, name=f'Active {i} auction',
                             **get_random_auction_data(get_current_random_dates), seller=seller, pending=False) for i in range(5, 10)]

    # Future pending auction
    auctions += [mixer.blend(AuctionProduct, name=f'Future {i} auction', seller=seller,
                             **get_random_auction_data(get_future_random_dates),
                             pending=random.choice([True, False])) for i in range(10)]

    # Past auctions
    auctions += [mixer.blend(AuctionProduct, name=f'Past {i} auction', seller=seller,
                             **get_random_auction_data(get_past_random_dates),
                             pending=random.choice([True, False])) for i in range(10)]

    return auctions


def add_coins():
    mixer.blend(CoinBundle, price=25, coins=50, sellable=True, purchase=None, image='images/coin_pile.png')
    mixer.blend(CoinBundle, price=50, coins=110, sellable=True, purchase=None, image='images/coin_pile.png')
    mixer.blend(CoinBundle, price=100, coins=225, sellable=True, purchase=None, image='images/coin_pile.png')


def main():
    seller, *rest = add_users()
    add_auctions(User.objects.get(username='seller1').bidder)
    add_coins()


main()
