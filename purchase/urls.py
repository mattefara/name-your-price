from django.urls import path

from .views import BundleView, BundlePurchase, BundlePurchaseDone

urlpatterns = [
    path('', BundleView.as_view(), name='bundle-view'),
    path('verify/<int:pk>', BundlePurchase.as_view(), name='bundle-purchase'),
    path('done/', BundlePurchaseDone.as_view(), name='bundle-purchase-done')
]
