import uuid

from django.contrib.auth.models import User
from django.db import models


class UserPurchase(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT, related_name='purchase')
    date = models.DateTimeField(auto_now_add=True, primary_key=True)

    class Meta:
        unique_together = (('user', 'date',),)

    @property
    def coins(self):
        if self.pk:
            return sum(bundle.coins for bundle in self.purchased_bundles.all())
        return 0

    def __str__(self):
        return f'{self.user} bought on {self.date}'


class CoinBundle(models.Model):
    title = models.CharField(max_length=150, name='title')
    coins = models.IntegerField(name='coins')
    price = models.IntegerField(name='price')
    sellable = models.BooleanField(name='sellable', default=False)
    purchase = models.ForeignKey(UserPurchase, on_delete=models.PROTECT, related_name='purchased_bundles', null=True)
    image = models.ImageField(upload_to='media/', default=None, null=True)
    bundle_uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    done = models.BooleanField(default=False)

    @staticmethod
    def add_to_user(user: User, title, coins, price, **kwargs):
        purchase = UserPurchase.objects.create(user=user)
        return CoinBundle.objects.create(title=title, coins=coins, price=price, purchase=purchase, **kwargs)

    def __str__(self):
        if not self.sellable:
            return f'{self.purchase} ({self.title})'
        else:
            return f'Sellable {self.coins} coins bundle ({self.title})'
