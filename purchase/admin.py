from django.contrib import admin

from purchase.models import CoinBundle, UserPurchase


class CoinBundleInline(admin.TabularInline):
    model = CoinBundle


class UserPurchaseModelAdmin(admin.ModelAdmin):
    search_fields = ('user__username',)
    ordering = ('date',)
    inlines = [CoinBundleInline]


class CoinBundleModelAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_filter = ('done', 'sellable')
    ordering = ('title',)


admin.site.register(UserPurchase, UserPurchaseModelAdmin)
admin.site.register(CoinBundle, CoinBundleModelAdmin)
