import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView

from .forms import PurchaseUUIDForm
from .models import CoinBundle, UserPurchase

_logger = logging.getLogger(__name__)


class BundleView(LoginRequiredMixin, ListView):
    model = CoinBundle
    template_name = 'purchase/bundle_view.html'

    def get_queryset(self):
        return self.model.objects.filter(sellable=True)


class BundlePurchase(LoginRequiredMixin, DetailView):
    model = CoinBundle

    def get_queryset(self):
        return self.model.objects.filter(done=False)

    def get_object(self, queryset=None):
        purchase_form = PurchaseUUIDForm(self.request.GET)
        if purchase_form.is_valid():
            bundle: CoinBundle = super(BundlePurchase, self).get_object(queryset)
            if not bundle.sellable:
                return None
            purchased_bundle = CoinBundle.objects.create(title=bundle.title, coins=bundle.coins, price=bundle.price)
            return purchased_bundle
        return None

    def get_context_data(self, **kwargs):
        context = super(BundlePurchase, self).get_context_data(**kwargs)
        if self.object:
            self.request.session['purchase_id'] = self.object.bundle_uuid.hex
        return context

    def render_to_response(self, context, **response_kwargs):
        if not self.object:
            raise Http404('Purchase not found')

        return redirect(reverse_lazy('bundle-purchase-done'))


class BundlePurchaseDone(LoginRequiredMixin, DetailView):

    model = CoinBundle
    template_name = 'purchase/bundle_purchase.html'

    def get_object(self, queryset=None):
        purchase_form = PurchaseUUIDForm(self.request.session)
        valid = purchase_form.is_valid()
        if self.request.session.get('purchase_id'):
            del self.request.session['purchase_id']
        if valid:
            purchase_uuid = purchase_form.cleaned_data.get('purchase_id')
            return self.model.objects.get(bundle_uuid=purchase_uuid)
        return None

    def render_to_response(self, context, **response_kwargs):
        if self.object:
            self.object.done = True
            self.object.save()
            user_purchase, _ = UserPurchase.objects.get_or_create(user=self.request.user)
            user_purchase.purchased_bundles.add(self.object)
        return super(BundlePurchaseDone, self).render_to_response(context=context, **response_kwargs)




