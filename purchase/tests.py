import uuid

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse_lazy

from .models import CoinBundle


def login_user(test_case: TestCase, username='test-user', password='test'):
    user = User.objects.get(username=username)
    logged_in = test_case.client.login(username=user.username, password=password)
    return logged_in, user


class UserPurchaseTestCase(TestCase):
    fixtures = ['coin_bundle.json']

    def setUp(self) -> None:
        User.objects.create_user('test-user', password='test')

    def test_login(self):
        logged_in, _ = login_user(self, 'test-user')
        self.assertTrue(logged_in)

    def test_purchase_coin_bundle(self):
        _, user = login_user(self, 'test-user')
        bundle = CoinBundle.objects.get(pk=1, sellable=True)
        path = reverse_lazy('bundle-purchase', kwargs={'pk': bundle.pk})
        response = self.client.get(path, data={'purchase_id': bundle.bundle_uuid}, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertGreater(user.purchase.coins, 0)

    def test_fail_purchase_coin_bundle(self):
        _, user = login_user(self, 'test-user')
        bundle = CoinBundle.objects.get(pk=1, sellable=True)
        path = reverse_lazy('bundle-purchase', kwargs={'pk': bundle.pk})
        response = self.client.get(path, data={'purchase_id': uuid.uuid4()}, follow=True)

        self.assertEqual(response.status_code, 404)

    def test_non_sellable_coin_bundle(self):
        _, user = login_user(self, 'test-user')
        bundle = CoinBundle.objects.create(title='', price=0, coins=100, sellable=False)
        path = reverse_lazy('bundle-purchase', kwargs={'pk': bundle.pk})
        response = self.client.get(path, data={'purchase_id': bundle.bundle_uuid}, follow=True)

        self.assertEqual(response.status_code, 404)
