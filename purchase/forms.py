from django import forms

from .models import CoinBundle


class PurchaseUUIDForm(forms.ModelForm):
    purchase_id = forms.UUIDField(required=True)

    class Meta:
        model = CoinBundle
        fields = ('purchase_id',)

    def is_valid(self):
        valid = super(PurchaseUUIDForm, self).is_valid()
        purchase_uuid = self.cleaned_data.get('purchase_id')
        try:
            _ = self.Meta.model.objects.get(bundle_uuid=purchase_uuid)
        except self.Meta.model.DoesNotExist:
            return False
        return valid
