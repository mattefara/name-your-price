from asgiref.sync import sync_to_async
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse_lazy
from django.utils import timezone
from mixer.backend.django import mixer

from auction.errors_codes import AuthErrorsCodes
from auction.models import AuctionProduct, Bid
from auction.tasks import auto_bid
from auction.tests import (
    WebSocketCommunicatorTestCase
)
from name_your_price.asgi import application as asgi_app
from purchase.models import CoinBundle


class BiddingAuctionTestCase(WebSocketCommunicatorTestCase):
    application = asgi_app

    def setUp(self) -> None:
        self.seller = mixer.blend(User, password=make_password('password')).bidder
        self.auction = mixer.blend(AuctionProduct, date_start=timezone.now(),
                                   date_end=timezone.now() + timezone.timedelta(hours=1),
                                   seller=self.seller, pending=False)
        self.user_1 = mixer.blend(User, password=make_password('password'))
        self.user_2 = mixer.blend(User, password=make_password('password'))

        self.bundle = mixer.blend(CoinBundle, price=0, coins=1_000_000, sellable=True)

    @sync_to_async
    def async_get(self, path, data, client=None, **kwargs):
        if not client:
            client = self.client
        return client.get(path, data=data, **kwargs)

    @sync_to_async
    def async_auto_bid(self, auction):
        return auto_bid(auction.pk)

    async def test_bid_single_bidder(self):
        await self.async_login(username=self.user_1.username, password='password')

        path = reverse_lazy('bundle-purchase', kwargs={'pk': self.bundle.pk})
        await self.async_get(path, data={'purchase_id': self.bundle.bundle_uuid}, follow=True)

        @sync_to_async
        def get_user_coins(user):
            return user.purchase.coins

        communicator = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", self.get_headers())
        await communicator.connect()

        await communicator.send_json_to({'amount': 1})
        data = await communicator.receive_json_from()

        await communicator.disconnect()
        self.assertIsNone(data.get('errors', None))

    async def test_bid_multiples_bidder(self):
        client_1 = Client()
        await self.async_login(client=client_1, username=self.user_1.username, password='password')
        await self.async_login(username=self.user_2.username, password='password')

        path = reverse_lazy('bundle-purchase', kwargs={'pk': self.bundle.pk})
        await self.async_get(path, data={'purchase_id': self.bundle.bundle_uuid}, follow=True)
        await self.async_get(path, data={'purchase_id': self.bundle.bundle_uuid}, client=client_1, follow=True)

        communicator_1 = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", self.get_headers())
        await communicator_1.connect()
        await communicator_1.send_json_to({'amount': 1})

        communicator_2 = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/",
                                               self.get_headers(client=client_1))
        await communicator_2.connect()

        await communicator_2.send_json_to({'amount': 2})
        data = await communicator_2.receive_json_from()

        self.assertIsNone(data.get('errors', None))

        await communicator_1.disconnect()
        await communicator_2.disconnect()

    async def test_auto_bid(self):
        client_1 = Client()
        await self.async_login(client=client_1, username=self.user_1.username, password='password')
        await self.async_login(username=self.user_2.username, password='password')

        path = reverse_lazy('bundle-purchase', kwargs={'pk': self.bundle.pk})
        await self.async_get(path, data={'purchase_id': self.bundle.bundle_uuid}, follow=True)
        await self.async_get(path, data={'purchase_id': self.bundle.bundle_uuid}, client=client_1, follow=True)

        communicator_1 = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", self.get_headers())
        await communicator_1.connect()
        await communicator_1.send_json_to({
            'amount': 1,
            'auto_bid': True,
            'auto_bid_amount': 10,
            'step': 1
        })
        _ = await communicator_1.receive_json_from()

        communicator_2 = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/",
                                               self.get_headers(client=client_1))
        await communicator_2.connect()

        await communicator_2.send_json_to({
            'amount': 2,
            'auto_bid': True,
            'auto_bid_amount': 10,
            'step': 1
        })
        data = await communicator_2.receive_json_from()

        self.assertIsNone(data.get('errors', None))

        await communicator_1.disconnect()
        await communicator_2.disconnect()

        while await self.async_auto_bid(self.auction):
            pass

        @sync_to_async
        def get_updated_auction(**kwargs):
            return AuctionProduct.objects.get(**kwargs).last_bidder

        @sync_to_async
        def get_updated_auction_bids(**kwargs):
            return [bid.amount for bid in Bid.objects.filter(**kwargs)]

        self.assertEqual(await get_updated_auction(pk=self.auction.pk), self.user_1.bidder)
        self.assertEqual(await get_updated_auction_bids(auction=self.auction), list(range(10, 0, -1)))


class TestAutoBids(TestCase):

    def test_generate_auto_bids(self):
        client = Client()
        seller = mixer.blend(User).bidder
        user_1 = mixer.blend(User, password=make_password('password'))
        user_2 = mixer.blend(User, password=make_password('password'))
        bundle = mixer.blend(CoinBundle, price=0, coins=1_000_000, sellable=True)
        auction = mixer.blend(AuctionProduct, date_start=timezone.now(),
                              date_end=timezone.now() + timezone.timedelta(hours=1),
                              seller=seller, pending=False)

        client.login(username=user_1.username, password='password')
        self.client.login(username=user_2.username, password='password')

        path = reverse_lazy('bundle-purchase', kwargs={'pk': bundle.pk})
        self.client.get(path, data={'purchase_id': bundle.bundle_uuid}, follow=True)
        client.get(path, data={'purchase_id': bundle.bundle_uuid}, follow=True)

        auction.auction_bids.create(auto=True, amount=1, user_bid=user_1.bidder, step=1, max_offer=20)
        auction.auction_bids.create(auction=auction, auto=True, amount=2, user_bid=user_2.bidder, step=1, max_offer=21)

        auction.last_bidder = user_2.bidder
        auction.save()

        from auction.tasks import auto_bid
        succes = False
        for i in range(10):
            succes = auto_bid(1)

        self.assertTrue(succes)


class Test1(WebSocketCommunicatorTestCase):

    def setUp(self) -> None:
        seller = mixer.blend(User, password=make_password('password')).bidder
        self.user = mixer.blend(User, password=make_password('password'))
        self.auction = mixer.blend(AuctionProduct, name='Test', date_start=timezone.now(), date_end=timezone.now(),
                                   seller=seller, pending=False)

    async def test_consumer_login(self):
        await self.async_login(username=self.user.username, password='password')

        communicator = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", headers=self.get_headers())

        connected, _ = await communicator.connect()
        self.assertTrue(connected)

        await communicator.send_json_to({'amount': 1})
        data = await communicator.receive_json_from()
        self.assertNotErrorCode(data.get('errors'), AuthErrorsCodes.NO_LOGIN.value, 'User must be logged')
        await communicator.disconnect()
