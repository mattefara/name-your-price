from django.views.generic import ListView

from auction.models import AuctionProduct


class HomeView(ListView):
    template_name = 'home.html'

    def get_queryset(self):
        return sorted([auction for auction in AuctionProduct.objects.filter(pending=False)
                       if auction.has_started and not auction.expired and auction.total > 0], key=lambda a: a.total,
                      reverse=True)[:10]
