const createAlertMessage =  (message, severity = 'danger') => {
    const alert = $('#alert-template').clone().attr('id', null)
    const messageSpan = alert.removeClass('hide').addClass(['show', `alert-${severity}`]).children('#alert-template-message')
    messageSpan.text(`${message}`)
    return alert
}

$(document).ready(() => {
    const updateBid = ({ date, amount, username, balance}) => {
        const biddingMessage = `${(currentUser === username) ? 'You' : username} bid ${amount}`

        if (currentUser === username){
            $('.total-coins').text(balance)
            $('.last-bid').text(amount)
        }
        $('.total-amount').text(amount)
        $('.last-bidder').text(username)
        $('#bidders-list').prepend(`<li class="list-group-item"> ${biddingMessage}</>`)
    }

    setInterval(() => {
        const auctions = $('.auction-end')
        auctions.each((i, auction) => {

            const auctionDateStart = new Date($($('.auction-start')[i]).text()).getTime()
            const time = new Date($(auction).text())

            const now = new Date().getTime();
            if (now < auctionDateStart){
                $($('.count-down-time')[i]).text("It will start");

            } else {
                const distance = time - now;

                const days = Math.floor(distance / (1000 * 60 * 60 * 24));
                const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                const seconds = Math.floor((distance % (1000 * 60)) / 1000);

                $($('.count-down-time')[i]).text(days + "d " + hours + "h " + minutes + "m " + seconds + "s ")
                if (distance < 0) {
                    $($('.count-down-time')[i]).text("Expired");
                }
            }
        })
    }, 1000)

    try {
        if (room){
            const SOCKET_URL = `ws://${window.location.host}/ws/auction-channel/${room}/`
            const socket = new WebSocket(SOCKET_URL)

            socket.onmessage = event => {
                console.log(JSON.parse(event.data))
                const parsedData = JSON.parse(event.data)
                if (parsedData && parsedData.errors){
                    const {errors} = parsedData
                    if (!Array.isArray(errors)) {
                        const parsedErrors = JSON.parse(errors)
                        const keys = Object.keys(parsedErrors)
                        keys.forEach((key) => {
                            parsedErrors[key].forEach(({ message }) => {
                                $('.messages-container').prepend(createAlertMessage(message))
                            } )
                        })
                    } else {
                        errors.forEach(({ message }) => {
                            const alert = createAlertMessage(message)
                            $('.messages-container').append(alert)
                        })
                    }
                } else if (parsedData && parsedData.payload) {
                    updateBid(parsedData.payload.data)
                }
            }

            socket.close = socket.onerror = () => {
                window.location.reload()
            }

            document.querySelector('#send-bid').addEventListener('click', (event) => {
                event.preventDefault()
                const message = {
                    amount : document.querySelector('#id_amount').value || 0,
                    auto_bid : $('#id_auto_bid').prop('checked'),
                    auto_bid_amount : $('#id_auto_bid_amount').prop('value') || 0,
                    step: $('#id_step').prop('value')
                }
                socket.send(JSON.stringify(message))
            })
        }
    } catch (e) {}

    $('#id_auto_bid').on('click', () => {
        const checked = $('#id_auto_bid').prop('checked')
        $('#id_auto_bid_amount').prop('disabled', !checked)
        $('#id_step').prop('disabled', !checked)
    })

    $('#advanced-search').on('click', () => {
        const searchContainer = $('#search-type')
        const searchType = searchContainer.text()
        if (searchType === 'normal'){
            searchContainer.text('advanced')
            const advancedSearchContainer = $('#id_advanced_search')
            advancedSearchContainer.prop('checked', !advancedSearchContainer.prop('checked'))
        } else {
            $('#search-type').text('normal')
        }

        ['category', 'sold', 'price'].forEach(field => {
            $(`#div_id_${field}`).toggle()
        })
    })
})