$(document).ready(() => {

    const updateChat = ( {text, username} ) => {
        $('#no-message-error').text('')
        $('#chat-message-list').append(`<li class="list-group-item"> ${username}: ${text}</>`)
    }

    const updateScroll = () => {
        const el = $('#chat-message-list')[0]
        el.scrollTop = el.scrollHeight
    }



    if (chatRoom){
        const SOCKET_URL = `ws://${window.location.host}/ws/chat-channel/${chatRoom}/`
        const socket = new WebSocket(SOCKET_URL)

        socket.onmessage = event => {
            const parsedData = JSON.parse(event.data)
            if (parsedData && parsedData.errors){
                const {errors} = parsedData
                errors.forEach(({message}) => {
                    const alert = createAlertMessage(message, 'warning')
                    $('.messages-container').append(alert)
                })
            } else if (parsedData && parsedData.payload) {
                const {payload} = parsedData
                updateChat(payload)
                updateScroll()
            }
        }

        $('#send-message').on('click', (event) => {
            event.preventDefault()
            const data = {
                text : $('#id_text').val()
            }
            socket.send(JSON.stringify(data))
        })
    }
})