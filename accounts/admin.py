from django.contrib import admin
from django.contrib.auth.models import User

from .models import AvatarUser


class AvatarUserAdmin(admin.TabularInline):
    model = AvatarUser


class UserAdmin(admin.ModelAdmin):
    inlines = (AvatarUserAdmin,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
