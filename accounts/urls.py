from django.urls import path

from .views import AccountCreateView, AccountLoginView, AccountLogoutView, UserProfileView, UserChangePasswordView

urlpatterns = [
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('register/', AccountCreateView.as_view(), name='register'),
    path('profile/', UserProfileView.as_view(), name='profile'),
    path('password/', UserChangePasswordView.as_view(), name='change-password'),
]
