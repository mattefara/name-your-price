from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages import SUCCESS
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView

from .forms import UpdateAvatarUserForm
from .mixins import MessageMixin
from .models import AvatarUser


class AccountLoginView(MessageMixin, LoginView):
    message = 'Successfully logged in'
    message_level = SUCCESS

    def form_valid(self, form):
        self.add_message(self.request, is_error=False)
        return super(AccountLoginView, self).form_valid(form)


class AccountLogoutView(MessageMixin, LogoutView):
    message = 'Successfully logged out'
    message_level = SUCCESS
    next_page = 'home'

    def dispatch(self, request, *args, **kwargs):
        response = super(AccountLogoutView, self).dispatch(request, *args, **kwargs)
        self.add_message(request, False)
        return response


class AccountCreateView(MessageMixin, CreateView):
    form_class = UserCreationForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('home')
    message = 'Account successfully created'
    message_level = SUCCESS

    def form_valid(self, form):
        response = super(AccountCreateView, self).form_valid(form)
        self.add_message(self.request, is_error=False)
        login(self.request, self.object)
        return response


class UserProfileView(MessageMixin, LoginRequiredMixin, UpdateView):

    model = AvatarUser
    form_class = UpdateAvatarUserForm
    template_name = 'registration/profile.html'
    success_url = '/'
    message = 'Account successfully updated'
    message_level = SUCCESS

    def get_object(self, queryset=None):
        return self.request.user
    
    def form_valid(self, form):
        self.request.user.avatar_user.avatar = form.cleaned_data.get('avatar')
        self.request.user.avatar_user.save()
        return super(UserProfileView, self).form_valid(form)

    def get_success_url(self):
        self.add_message(self.request, is_error=False)
        return super(UserProfileView, self).get_success_url()


class UserChangePasswordView(LoginRequiredMixin, MessageMixin, PasswordChangeView):

    model = AvatarUser
    form_class = PasswordChangeForm
    message = 'Account successfully updated'
    message_level = SUCCESS
    template_name = 'registration/password_change.html'

    def get_queryset(self):
        return AvatarUser.objects.get(user=self.request.user)

    def get_success_url(self):
        self.add_message(self.request, is_error=False)
        return reverse_lazy('home')
