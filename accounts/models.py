from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class AvatarUser(models.Model):

    user = models.OneToOneField(User, on_delete=models.PROTECT, related_name='avatar_user')
    avatar = models.ImageField(upload_to='images/', default=None)


@receiver(post_save, sender=User)
def create_avatar_user(sender, instance, created,  **kwargs):
    if created:
        AvatarUser.objects.create(user=instance)