from django.contrib import messages
from django.contrib.messages import INFO, ERROR


class MessageMixin:
    message: str
    message_error: str
    message_level: int = INFO
    message_level_error = ERROR

    def add_message(self, request, is_error=False):
        if is_error:
            messages.add_message(request, self.message_level_error, self.message_level)
        else:
            messages.add_message(request, self.message_level, self.message)
