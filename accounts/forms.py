from django import forms
from django.contrib.auth.forms import UserChangeForm, ReadOnlyPasswordHashField

from .models import AvatarUser


class UpdateAvatarUserForm(UserChangeForm):

    username = forms.CharField(disabled=True, required=False)
    first_name = forms.CharField()
    last_name = forms.CharField()
    avatar = forms.ImageField()
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = AvatarUser
        fields = ('username', 'first_name', 'last_name', 'avatar')
