from enum import Enum


class AuthErrorsCodes(Enum):
    NO_LOGIN = 1000


class AuctionErrorsCodes(Enum):
    AUCTION_DOES_NOT_EXISTS = 2000
    AUCTION_EXPIRED = 2001
    AUCTION_PENDING = 2002


class BidderErrorsCodes(Enum):
    LAST_BIDDER_MADE_BID = 3000
    SELLER_MADE_BID = 3001
    HIGH_AUTO_BID = 3002
    LOW_BALANCE = 3003
    INSUFFICIENT_AMOUNT = 3004


class PayloadErrorCodes(Enum):
    INVALID_PAYLOAD = 4000
