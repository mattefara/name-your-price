import datetime
import json

from asgiref.sync import sync_to_async
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.testing import WebsocketCommunicator
from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, TransactionTestCase
from django.urls import reverse_lazy
from django.utils import timezone
from mixer.backend.django import mixer

from .errors_codes import AuthErrorsCodes, AuctionErrorsCodes, BidderErrorsCodes
from .models import AuctionProduct
from .routing import ws_urlpatterns


class AuctionTestCase(TestCase):

    def setUp(self) -> None:
        mixer.blend(User, username='test-user')

    def test_bidder(self):
        bidder = User.objects.get(username='test-user').bidder
        self.assertNotEqual(bidder, None)

    def test_coins(self):
        bidder = User.objects.get(username='test-user').bidder
        self.assertEqual(bidder.balance, 0)


class AuctionTestView(TestCase):

    def setUp(self) -> None:
        mixer.blend(User, username='test-user', password=make_password('password'))

    def test_get_create_auction_page(self):
        user = User.objects.get(username='test-user')
        self.client.login(username=user.username, password='password')
        response = self.client.get(reverse_lazy('sell'))
        self.assertEqual(response.status_code, 200)

    def test_create_auction(self):
        user = User.objects.get(username='test-user')
        self.client.login(username=user.username, password='password')
        image_path = settings.MEDIA_ROOT / 'images' / 'immagine_test_car.jpg'
        form_data = {
            "name": "Test auction",
            "description": "Description test",
            "date_start": timezone.now(),
            "date_end": timezone.now() + datetime.timedelta(days=1),
            "category": "NO",
            "product_image": SimpleUploadedFile('test_image.jpg',
                                                content=open(image_path, 'rb').read(),
                                                content_type='image/jpeg')
        }

        response = self.client.post(reverse_lazy('sell'), data=form_data)
        self.assertRedirects(response, reverse_lazy('sell-pending'), status_code=302, target_status_code=200)
        self.assertNotEqual(AuctionProduct.objects.get(name='Test auction'), None)
        self.assertRedirects(response, reverse_lazy('sell-pending'))

    def test_error_create_auction(self):
        user = User.objects.get(username='test-user')
        self.client.login(username=user.username, password='password')
        image_path = settings.MEDIA_ROOT / 'images' / 'immagine_test_car.jpg'
        form_data = {
            "name": "Test auction 2",
            "date_start": timezone.now(),
            "date_end": timezone.now() - datetime.timedelta(days=1),
            "product_image": SimpleUploadedFile('test_image.jpg',
                                                content=open(image_path, 'rb').read(),
                                                content_type='image/jpeg')
        }

        response = self.client.post(reverse_lazy('sell'), data=form_data)
        self.assertEqual(response.status_code, 200)


class WebSocketCommunicatorTestCase(TransactionTestCase):
    application = None
    paths = ws_urlpatterns

    @sync_to_async
    def async_login(self, client=None, **credentials):
        if not client:
            client = self.client
        logged = client.login(**credentials)
        self.assertTrue(logged)
        return logged

    def get_headers(self, client=None):
        if not client:
            client = self.client
        return [(b'origin', b'...'), (b'cookie', client.cookies.output(header='', sep='; ').encode())]

    def get_communicator(self, path, headers):
        app = self.application or ProtocolTypeRouter({'websocket': AuthMiddlewareStack(URLRouter(self.paths))})
        return WebsocketCommunicator(app, path, headers)

    def assertNotErrorCode(self, error, code, msg=''):
        if not isinstance(error, list):
            self.assertNotEqual(error.get('code', None), error, msg=msg)
        for e in error:
            err_code = e.get('code', None)
            if not err_code:
                raise self.failureException('Error code must be provided')
            self.assertNotEqual(err_code, code, msg=msg)

    def assertErrorCode(self, error, code, message=''):
        if not isinstance(error, list):
            self.assertNotEqual(error.get('code', None), error, msg=message)
        for e in error:
            err_code = e.get('code', None)
            if not err_code:
                raise self.failureException('Error code must be provided')
            if not message:
                message = f'Expecting error code {code} buf found {err_code}'
            self.assertEqual(err_code, code, msg=message)


class JsonAuctionWebSocketTestCase(WebSocketCommunicatorTestCase):
    application = None

    def setUp(self) -> None:
        self.seller = mixer.blend(User, password=make_password('password')).bidder
        self.user = mixer.blend(User, password=make_password('password'))
        self.auction = mixer.blend(AuctionProduct, datte_start=timezone.now(),
                                   date_end=timezone.now() + timezone.timedelta(hours=1),
                                   seller=self.seller, pending=False)
        self.expired_auction = mixer.blend(AuctionProduct, datte_start=timezone.now(),
                                           date_end=timezone.now(), seller=self.seller, pending=False)

    async def test_consumer_login(self):
        logged = await self.async_login(username=self.user.username, password='password')
        self.assertTrue(logged)

        communicator = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", self.get_headers())
        connected, _ = await communicator.connect()
        self.assertTrue(connected)

        await communicator.send_json_to({'amount': 1})
        data = await communicator.receive_json_from()
        self.assertNotErrorCode(data.get('errors'), AuthErrorsCodes.NO_LOGIN.value, 'User must be logged')
        await communicator.disconnect()

    async def test_error_negative_amount_bid(self):
        await self.async_login(username=self.auction.seller.user.username, password='password')
        communicator = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", self.get_headers())
        await communicator.connect()
        await communicator.send_json_to({'amount': -1})
        data = await communicator.receive_json_from()
        self.assertIsNotNone(json.loads(data.get('errors', {})).get('amount', None), msg='Testing an invalid payload')
        await communicator.disconnect()

    async def test_expired_auction(self):
        await self.async_login(username=self.user.username, password='password')
        communicator = self.get_communicator(f"/ws/auction-channel/{self.expired_auction.pk}/", self.get_headers())
        await communicator.connect()
        await communicator.send_json_to({'amount': 1})
        data = await communicator.receive_json_from()
        self.assertErrorCode(data.get('errors'), AuctionErrorsCodes.AUCTION_EXPIRED.value)
        await communicator.disconnect()

    async def test_error_seller_place_bid(self):
        await self.async_login(username=self.auction.seller.user.username, password='password')
        communicator = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", self.get_headers())
        await communicator.connect()
        await communicator.send_json_to({'amount': 1})
        data = await communicator.receive_json_from()
        self.assertErrorCode(data.get('errors'), BidderErrorsCodes.SELLER_MADE_BID.value)
        await communicator.disconnect()

    async def test_low_balance(self):
        await self.async_login(username=self.user.username, password='password')
        communicator = self.get_communicator(f"/ws/auction-channel/{self.auction.pk}/", self.get_headers())
        await communicator.connect()
        await communicator.send_json_to({'amount': 2})
        data = await communicator.receive_json_from()
        self.assertErrorCode(data.get('errors'), BidderErrorsCodes.LOW_BALANCE.value)
        await communicator.disconnect()
