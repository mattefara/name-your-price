import logging

from asgiref.sync import async_to_sync
from celery import shared_task
from channels.layers import get_channel_layer
from django_celery_beat.models import PeriodicTask

from .models import AuctionProduct, Bid

_logger = logging.getLogger(__name__)

current_channel_layer = get_channel_layer()


@shared_task(bind=True)
def auto_bid(task, auction_id, *args, **kwargs):
    auction = AuctionProduct.objects.get(pk=auction_id)
    if auction.expired:
        return False

    for auto_bidder in auction.auto_bidders.all():

        # Checking if last_bidder is the current auto_bidder
        if auto_bidder == auction.last_bidder:
            continue

        # Latest automatic bid from the current auto_bidder
        oldest_bid_query = Bid.objects.filter(user_bid=auto_bidder, auction=auction)
        if auto_bidder.balance > 0 and oldest_bid_query.exists():
            oldest_bid = oldest_bid_query.first()
            if not oldest_bid.auto:
                auction.auto_bidders.remove(auto_bidder)
                continue
            next_amount = auction.total + oldest_bid.step

            # Check if user can make next bid
            allowed = next_amount <= oldest_bid.max_offer and next_amount <= auto_bidder.balance
            if allowed:
                # Create new automatic bid
                bid = Bid.objects.create(auction=auction, user_bid=auto_bidder, auto=True, amount=next_amount,
                                         max_offer=oldest_bid.max_offer, step=oldest_bid.step)
                auction.auction_bids.add(bid)
                auction.last_bidder = auto_bidder
                auction.save()

                async_to_sync(current_channel_layer.group_send)(f'auction_{auction_id}', {
                    'type': 'bid',
                    'payload': {
                        'data': {
                            'amount': next_amount,
                            'date': bid.date.strftime('%b %d, %Y %H:%M:%S')
                        }
                    },
                })
                return True
            else:
                auction.auto_bidders.remove(auto_bidder)
        else:
            auction.auto_bidders.remove(auto_bidder)
    auction.save()
    return False


@shared_task(bind=True)
def stop_auto_bid_task(task, periodic_task_id, **kwargs):
    periodic_task = PeriodicTask.objects.get(id=periodic_task_id)
    periodic_task.enabled = False
    periodic_task.save()
