import copy
import json
import logging

from asgiref.sync import async_to_sync
from channels.db import database_sync_to_async
from channels.generic.websocket import WebsocketConsumer, AsyncJsonWebsocketConsumer
from django.views.generic.edit import FormMixin

from .errors_codes import AuctionErrorsCodes, AuthErrorsCodes, BidderErrorsCodes
from .forms import BiddingForm, ChatMessage
from .models import AuctionProduct, Bidder, Bid

logging.basicConfig(format='%(asctime)s %(name)s %(levelname)s:%(message)s', level=logging.DEBUG)
_logger = logging.getLogger(__name__)


class AsyncJsonWebsocketConsumerForm(AsyncJsonWebsocketConsumer, FormMixin):

    def __init__(self):
        super().__init__()
        self.payload = {}

    async def receive_json(self, content, **kwargs):
        self.payload = content
        form = self.get_form()
        if form.is_valid():
            data = await self.form_valid(form)
        else:
            data = await self.form_invalid(form)

        return data

    @database_sync_to_async
    def form_valid(self, form):
        return {
            'data': form.cleaned_data
        }

    @database_sync_to_async
    def form_invalid(self, form):
        return {
            'errors': form.errors.as_json()
        }

    @staticmethod
    def send_error_message(message, code, prefix='message'):
        return {prefix: message, 'code': code}

    def get_form_kwargs(self):
        return {'data': self.payload}


class JsonAuctionConsumer(AsyncJsonWebsocketConsumerForm):
    form_class = BiddingForm

    def __init__(self):
        super().__init__()
        self.room_name = None
        self.group_name = None

    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.group_name = f'auction_{self.room_name}'

        await self.channel_layer.group_add(self.group_name, self.channel_name)
        return await super(JsonAuctionConsumer, self).connect()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)
        return await super(JsonAuctionConsumer, self).disconnect(code=code)

    async def receive_json(self, content, **kwargs):
        data = await super().receive_json(content, **kwargs)

        if data.get('errors', None) is not None:
            await self.send_json(data)
            return data

        await self.channel_layer.group_send(self.group_name, {
            'type': 'bid',
            'payload': data,
            'channel': self.channel_name
        })

        return data

    @database_sync_to_async
    def form_valid(self, form):

        user = self.scope['user']
        if not user.username:
            return {'errors': [self.send_error_message('User must be logged', AuthErrorsCodes.NO_LOGIN.value)]}

        auction = AuctionProduct.objects.filter(pk=self.room_name)
        if not auction.exists():
            return {'errors': [self.send_error_message('This auction does not exists',
                                                       AuctionErrorsCodes.AUCTION_DOES_NOT_EXISTS.value)]}

        auction = auction.get()
        if auction.pending:
            return {'errors': [self.send_error_message('This auction must be approved first',
                                                       AuctionErrorsCodes.AUCTION_PENDING.value)]}

        if auction.expired:
            return {'errors': [self.send_error_message('Auction expired', AuctionErrorsCodes.AUCTION_EXPIRED.value)]}

        if not auction.has_started:
            return {'errors': [self.send_error_message('Auction must start before bidding',
                                                      AuctionErrorsCodes.AUCTION_DOES_NOT_EXISTS.value)]}

        if auction.seller == user.bidder:
            return {
                'errors': [self.send_error_message('Seller cannot make bids', BidderErrorsCodes.SELLER_MADE_BID.value)]}

        if user.bidder == auction.last_bidder:
            return {'errors': [
                self.send_error_message('Last bidder cannot place bid', BidderErrorsCodes.LAST_BIDDER_MADE_BID.value)]}

        amount = form.cleaned_data.get('amount')
        auto_bid_amount = form.cleaned_data.get('auto_bid_amount')
        if not auction.last_bidder:
            if amount <= user.bidder.balance:
                if auto_bid_amount and auto_bid_amount > user.bidder.balance:
                    return {'errors': [
                        self.send_error_message('Auto bid amount is too high', BidderErrorsCodes.HIGH_AUTO_BID.value)]}
            else:
                return {'errors': [self.send_error_message('Cannot place bid if you have not enough money.',
                                                           BidderErrorsCodes.LOW_BALANCE.value)]}
        elif auction.last_bidder and auction.total < amount <= auction.last_bidder.balance:
            if auto_bid_amount and auto_bid_amount > auction.last_bidder.balance:
                return {'errors': self.send_error_message('Auto bid amount is too high.',
                                                          BidderErrorsCodes.HIGH_AUTO_BID.value)}
        elif auction.total == amount:
            return {'errors': [
                self.send_error_message('Insufficient bid amount', BidderErrorsCodes.INSUFFICIENT_AMOUNT.value)]}
        auto = form.cleaned_data.get('auto_bid', False)
        auto_data = {}
        if auto:
            auto_data = {
                'max_offer': auto_bid_amount,
                'step': form.cleaned_data.get('step', 0)
            }
        bid = Bid.objects.create(auction=auction, user_bid=user.bidder, amount=amount, auto=auto, **auto_data)
        user.bidder.bids.add(bid)
        user.bidder.save()

        auction.last_bidder = user.bidder
        auction.auction_bids.add(bid)
        auction.save()

        return {
            'data': {
                'username': user.username,
                'amount': amount,
                'date': bid.date.strftime('%b %d, %Y %H:%M:%S'),
                'balance': user.bidder.balance
            }
        }

    @database_sync_to_async
    def get_username(self):
        return AuctionProduct.objects.get(pk=self.room_name).last_bidder.user.username

    @database_sync_to_async
    def get_user_balance(self):
        return AuctionProduct.objects.get(pk=self.room_name).last_bidder.balance

    async def bid(self, event):
        payload = copy.deepcopy(event.get('payload', None))
        channel = event.get('channel', None)

        if channel != self.channel_name:
            payload['data'].pop('balance', None)

        if payload['data'].get('username', None) is None:
            username = await self.get_username()
            added_data = {'username': await self.get_username()}
            if username == self.scope['user'].username:
                added_data.update({'balance': await self.get_user_balance()})
            payload['data'].update(added_data)

        await self.send_json({
            'payload': payload
        })


class JsonChatConsumer(AsyncJsonWebsocketConsumerForm):
    form_class = ChatMessage

    def __init__(self):
        super().__init__()
        self.chat_room = None
        self.chat_group = None

    async def connect(self):
        self.chat_room = self.scope['url_route']['kwargs']['room_name']
        self.chat_group = f'chat_{self.chat_room}'

        await self.channel_layer.group_add(self.chat_group, self.channel_name)
        return await super().connect()

    async def receive_json(self, content, **kwargs):
        data = await super().receive_json(content, **kwargs)

        if data.get('errors', None) is not None:
            await self.send_json(data)
            return data

        await self.channel_layer.group_send(self.chat_group, {
            'type': f'send.message',
            'payload': {
                'text': data['data'].get('text'),
                'username': self.scope['user'].username,
            }
        })
        return data

    @database_sync_to_async
    def form_valid(self, form):
        user = self.scope['user']
        if not user.username:
            return {'errors': [self.send_error_message('User must be logged', AuthErrorsCodes.NO_LOGIN.value)]}

        auction = AuctionProduct.objects.get(pk=self.chat_room)
        if auction.expired:
            return {'errors': [self.send_error_message("Can\'t send message, auction expired", 1)]}
        if auction.has_started:
            return {'errors': [self.send_error_message("Can\'t send message, auction has started", 1)]}
        if not auction.pending:
            return {'errors': [self.send_error_message("Can\'t send message, auction is already approved", 1)]}

        text = form.cleaned_data.get('text')
        auction.messages.create(sender=self.scope['user'].bidder, text=text, receiver_id=1)

        return async_to_sync(super().form_valid)(form)

    async def send_message(self, event):
        payload = event['payload']
        await self.send_json({
            'payload': payload
        })


class ChatConsumer(WebsocketConsumer):

    def __init__(self):
        super(ChatConsumer, self).__init__()
        self.chat_room = None
        self.chat_group = None

    def connect(self):
        self.chat_room = self.scope['url_route']['kwargs']['room_name']
        self.chat_group = f'chat_{self.chat_room}'

        async_to_sync(self.channel_layer.group_add)(self.chat_group, self.channel_name)
        super(ChatConsumer, self).connect()

    def receive(self, text_data=None, bytes_data=None):
        parsed_data = json.loads(text_data)
        message_form = ChatMessage(parsed_data)
        if message_form.is_valid():

            auction = AuctionProduct.objects.get(pk=self.chat_room)
            if auction.has_started or auction.expired:
                self.send(json.dumps({
                    'errors': [
                        {'message': 'Chat can be used only before the auction starts'}
                    ]
                }))
                return super(ChatConsumer, self).receive(text_data, bytes_data)

            message = message_form.save(commit=False)
            message.chat_id = auction.id
            message.sender = self.scope['user'].bidder
            message.receiver, _ = Bidder.objects.get_or_create(user=self.scope['user'])

            message.save()

            auction.messages.add(message)
            auction.save()
            async_to_sync(self.channel_layer.group_send)(self.chat_group, {
                'type': f'send.message',
                'payload': {
                    'text': message_form.cleaned_data.get('text'),
                    'username': self.scope['user'].username,
                    # 'date': str(timezone.make_aware(timezone.datetime.now()))
                }
            })
        return super(ChatConsumer, self).receive(text_data, bytes_data)

    def send_message(self, event):
        payload = event['payload']

        self.send(text_data=json.dumps({
            'payload': payload
        }))
