from django.urls import path

from .views import SearchAuctionsListView, AuctionBidForm, AuctionSell, \
    AuctionHistory, AuctionSellList, UserReviewCreate, UserReviewList, AuctionChat, AuctionChatList, AuctionApprove, \
    UserReviewUpdate

urlpatterns = [
    path('search/', SearchAuctionsListView.as_view(), name='auction-list'),
    path('room/<int:pk>', AuctionBidForm.as_view(), name='auction-detail'),
    path('sell/', AuctionSell.as_view(), name='sell'),
    path('sell/pending', AuctionSellList.as_view(), name='sell-pending'),
    path('history/', AuctionHistory.as_view(), name='history'),
    path('reviews/<int:pk>', UserReviewList.as_view(), name='reviews'),
    path('reviews/create/<int:pk>', UserReviewCreate.as_view(), name='review-create'),
    path('reviews/update/<int:pk>', UserReviewUpdate.as_view(), name='review-update'),
    path('chat/<int:pk>', AuctionChat.as_view(), name='auction-chat'),
    path('chatlist', AuctionChatList.as_view(), name='auction-chat-list'),
    path('approve/<int:pk>', AuctionApprove.as_view(), name='auction-approve'),
]
