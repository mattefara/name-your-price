import json

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Avg, Max
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_celery_beat.models import PeriodicTask, IntervalSchedule, ClockedSchedule


class AuctionProduct(models.Model):
    class AuctionCategory(models.TextChoices):
        CARS = 'CA', _('Cars'),
        CLOCKS = 'CL', _('Clocks'),
        JEWELS = 'JE', _('Jewels'),
        TECHNOLOGY = 'TE', _('Technology'),
        NONE = 'NO', _('None'),

    name = models.CharField(max_length=128)
    date_start = models.DateTimeField(null=False)
    date_end = models.DateTimeField(null=False)
    description = models.TextField()
    product_image = models.ImageField(upload_to='images/')
    pending = models.BooleanField(default=True)
    seller = models.ForeignKey('Bidder', on_delete=models.PROTECT, related_name='auctioned_items')
    last_bidder = models.ForeignKey('Bidder', on_delete=models.PROTECT, null=True, default=None, unique=False)
    auto_bidders = models.ManyToManyField('Bidder', related_name='auto_auctions', default=None)
    category = models.CharField(
        max_length=2,
        choices=AuctionCategory.choices,
        default=AuctionCategory.NONE
    )

    @property
    def has_started(self):
        return self.date_start <= timezone.now() <= self.date_end

    @property
    def expired(self):
        return self.date_end <= timezone.now()

    @property
    def total(self):
        exist = self.auction_bids.exists()
        if exist:
            return self.auction_bids.aggregate(Max('amount')).get('amount__max')
        return 0

    @property
    def can_start(self):
        return not self.expired and not self.has_started

    def __str__(self):

        status = []
        if self.expired:
            status.append('Expired')
        if self.pending:
            status.append('Pending')
        if self.has_started:
            status.append('Started')

        joined = f"({', '.join(status)})" if status else ''
        return f'{self.name} {joined}'


class Bidder(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT, related_name='bidder')

    @property
    def last_bid(self):
        return Bid.objects.filter(user_bid=self).latest('date')

    def get_last_bid(self, auction: AuctionProduct):
        return auction.last_bidder.bids.filter(user_bid=self).aggregate(Max('amount'))

    @property
    def has_offered(self):
        return Bid().has_user_bid(self)

    @property
    def is_seller(self):
        return AuctionProduct.objects.filter(seller__user=self.user).count() > 0

    @property
    def has_ratings(self):
        return AuctionProduct.objects.filter(seller__user=self.user, ratings__isnull=False).count() > 0

    @property
    def average_ratings(self):
        return Rating.objects.filter(auction__seller_id=self).aggregate(Avg('vote'))

    @property
    def balance(self):
        if self.pk:
            auctions = [auction for auction in AuctionProduct.objects.filter(auction_bids__user_bid=self).distinct()]
            spent_coins = sum(Bid.objects.filter(auction=auction, user_bid=self).latest('date').amount
                              for auction in auctions)
            try:
                return self.user.purchase.coins - spent_coins
            except ObjectDoesNotExist:
                return 0
        return 0

    def __str__(self):
        return str(self.user)


class Bid(models.Model):
    amount = models.IntegerField()
    date = models.DateTimeField(auto_now=True)
    user_bid = models.ForeignKey(Bidder, models.PROTECT, related_name='bids')
    auction = models.ForeignKey(AuctionProduct, models.PROTECT, related_name='auction_bids')
    step = models.IntegerField(null=True, default=0)
    auto = models.BooleanField(default=False)
    max_offer = models.IntegerField(null=True, default=0)
    finished = models.BooleanField(default=False)

    def has_user_bid(self, user):
        return Bid.objects.filter(user_bid=user, auction=self.auction)

    def __str__(self):
        return f'{self.user_bid} at {self.auction} on {self.date} ({self.amount})'

    class Meta:
        ordering = ('-date',)


class Rating(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    auction = models.OneToOneField(AuctionProduct, on_delete=models.PROTECT, related_name='rating', default=None)
    bidder_rating = models.ForeignKey(Bidder, on_delete=models.PROTECT, related_name='bidder_ratings')
    vote = models.IntegerField()

    def __str__(self):
        return f'{self.bidder_rating} voted {self.auction}'


class Message(models.Model):
    chat = models.ForeignKey(AuctionProduct, on_delete=models.PROTECT, related_name='messages', default=None)
    text = models.CharField(max_length=265)
    sender = models.ForeignKey(Bidder, unique=False, on_delete=models.PROTECT, related_name='sent_messages')
    receiver = models.ForeignKey(Bidder, unique=False, on_delete=models.PROTECT, related_name='received_messages')
    date = models.DateTimeField(default=timezone.datetime.now)

    def __str__(self):
        return self.text


@receiver(post_save, sender=User)
def add_bidder(sender, instance, created, **kwargs):
    if created:
        Bidder.objects.create(user=instance)


@receiver(post_save, sender=Bid)
def place_auto_bid(sender, instance: Bid, created, **kwargs):
    if instance.auto:
        instance.auction.auto_bidders.add(instance.user_bid)
        instance.auction.save()

    auction = instance.auction
    if auction.last_bidder != instance.user_bid:
        auction.last_bidder = instance.user_bid
        auction.save()


@receiver(post_delete, sender=Bid)
def place_auto_bid(sender, instance: Bid, **kwargs):
    auction = instance.auction
    if auction.last_bidder == instance.user_bid:
        auction.last_bidder = None
        auction.save()
    if instance.user_bid in auction.auto_bidders.all():
        auction.auto_bidders.remove(instance.user_bid)
        auction.save()


@receiver(post_save, sender=Bid)
def subscribe_auto_bidder(sender, instance: Bid, **kwargs):
    if not instance.auto:
        return
    auction = instance.auction
    if instance.user_bid in auction.auto_bidders.all():
        auction.auto_bidders.add(instance.user_bid)
        auction.save()


@receiver(post_save, sender=AuctionProduct)
def add_periodic_task(sender, instance: AuctionProduct, **kwargs):
    if not instance.expired and not instance.pending:
        task_name = f'auction-{instance.id}-task'
        task_query = PeriodicTask.objects.filter(name=task_name)
        if task_query.exists():
            return
        schedule, _ = IntervalSchedule.objects.get_or_create(every=10, period=IntervalSchedule.SECONDS)
        periodic_task, _ = PeriodicTask.objects.get_or_create(interval=schedule, name=task_name,
                                                              task='auction.tasks.auto_bid',
                                                              args=json.dumps([instance.id]),
                                                              expires=instance.date_end)

        run_time = ClockedSchedule.objects.create(clocked_time=instance.date_end)
        PeriodicTask.objects.get_or_create(clocked=run_time, name=f'auction-{instance.id}-cancel-task',
                                           start_time=instance.date_start,
                                           task='auction.tasks.stop_auto_bid_task', args=json.dumps([periodic_task.id]),
                                           one_off=True)


@receiver(post_delete, sender=AuctionProduct)
def remove_tasks(sender, instance: AuctionProduct, **kwargs):
    task = PeriodicTask.objects.filter(name=f'{instance}-{instance.id}-task')
    if task.exists():
        task.get().delete()
        PeriodicTask.objects.get(name=f'{instance}-{instance.id}-cancel-task').delete()
