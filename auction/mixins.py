from urllib.parse import urlencode

from django.views.generic import ListView
from django.views.generic.edit import FormMixin


class SearchFormView(FormMixin, ListView):

    def get_context_data(self, **kwargs):
        context = super(SearchFormView, self).get_context_data(**kwargs)
        form = kwargs.get('form', None)
        context.update({
            'last_search': form.cleaned_data if form else None,
            'query': urlencode(form.cleaned_data if form else '')
        })
        context.update(self.kwargs)
        return context

    def get_queryset(self):
        form = self.get_form()
        if form.is_valid():
            queryset = self.form_valid(form)
        else:
            queryset = self.form_invalid(form)

        return queryset

    def get_form_kwargs(self):
        kwargs = super(SearchFormView, self).get_form_kwargs()
        if self.request.method in 'GET':
            kwargs.update({
                'data': self.request.GET
            })
        return kwargs

    def form_valid(self, form):
        return self.model.obejcts.all()

    def form_invalid(self, form):
        return self.model.objects.all()
