import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages import INFO, ERROR
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import ListView, DetailView, CreateView, FormView
from django.views.generic.edit import FormMixin, UpdateView

from .forms import BiddingForm, SearchBiddingForm, CreateAuctionProductForm, AuctionOrderingForm, ReviewCreationForm, \
    ChatMessage, SearchForm
from .mixins import SearchFormView
from .models import AuctionProduct, Bidder, Rating

_logger = logging.getLogger(__name__)


class SearchAuctionsListView(SearchFormView):
    model = AuctionProduct
    form_class = SearchBiddingForm
    template_name = 'auction/auction_list.html'
    paginate_by = 10
    ordering = ('-date_end', 'name')

    def form_valid(self, form):
        query_dict = form.cleaned_data
        if query_dict.get('all_categories', False):
            category = ''
        else:
            category = query_dict.get('category')

        name = query_dict.pop('name', '')
        price = query_dict.get('price') or 0

        sold = query_dict.get('sold')
        if sold:
            date_options = {'date_end__lt': timezone.make_aware(timezone.datetime.now())}
        else:
            date_options = {'date_end__gt': timezone.make_aware(timezone.datetime.now())}

        price_query = Q(auction_bids__amount__gte=price)

        if price == 0:
            price_query |= Q(auction_bids__amount__isnull=True)

        return self.model.objects.filter(price_query, pending=False, name__icontains=name, **date_options,
                                         category__contains=category) \
            .distinct().order_by('-last_bidder', 'date_start', 'date_end', '-name')


class AuctionBidForm(LoginRequiredMixin, FormMixin, DetailView):
    login_url = 'login'
    form_class = BiddingForm
    model = AuctionProduct
    template_name = 'auction/auction_detail.html'

    def get(self, request, *args, **kwargs):
        user = request.user
        bidder, created = Bidder.objects.get_or_create(user=user)
        auction = get_object_or_404(AuctionProduct, pk=self.kwargs['pk'])
        if auction.seller == bidder or bidder.balance > 0:
            return super(AuctionBidForm, self).get(request, *args, **kwargs)
        return redirect(reverse_lazy('bundle-view'))


class AuctionSell(LoginRequiredMixin, CreateView):
    model = AuctionProduct
    form_class = CreateAuctionProductForm
    template_name = 'auction/auction_sell.html'

    def form_valid(self, form):
        auction = form.save(commit=False)
        auction.seller = self.request.user.bidder
        auction.save()
        return super(AuctionSell, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('sell-pending')


class AuctionSellList(LoginRequiredMixin, ListView):
    model = AuctionProduct
    template_name = 'auction/auction_pending.html'
    paginate_by = 10

    def get_queryset(self):
        return self.model.objects.filter(seller=self.request.user.bidder, pending=True)


class AuctionHistory(LoginRequiredMixin, SearchFormView):
    model = AuctionProduct
    template_name = 'auction/auction_history.html'
    ordering = ('name',)
    paginate_by = 15
    form_class = AuctionOrderingForm

    def form_valid(self, form):
        query_dict = form.cleaned_data
        query = Q(auction_bids__user_bid=self.request.user.bidder) | Q(seller=self.request.user.bidder) | Q(
            last_bidder=self.request.user.bidder)
        is_seller = query_dict.get('is_seller', False)
        if is_seller:
            query = Q(seller=self.request.user.bidder)
        return self.model.objects.filter(query).distinct().order_by(*self.get_ordering(form=form))

    def get_ordering(self, **kwargs):
        query_dict = kwargs.get('form').cleaned_data
        ordering = query_dict.get('ordering', 'name')
        if ordering == 'expired':
            ordering = 'date_end'
        descending = not query_dict.get(
            'descending') if ordering == 'name' else query_dict.get('descending')
        if descending:
            return '-' + ordering,
        else:
            return ordering,


class UserReviewCreate(LoginRequiredMixin, FormView):
    model = Rating
    form_class = ReviewCreationForm
    template_name = 'ratings/rating_create.html'

    def form_valid(self, form):
        auction = AuctionProduct.objects.get(pk=self.kwargs.get('pk'))
        if auction.expired and auction.last_bidder == self.request.user.bidder:
            try:
                _ = auction.rating
                messages.add_message(self.request, ERROR, "Cannot send feedback because it was sent previously")
            except Rating.DoesNotExist:
                rating = form.save(commit=False)
                rating.auction = auction
                rating.bidder_rating = self.request.user.bidder
                rating.save()
            return super(UserReviewCreate, self).form_valid(form)
        raise PermissionDenied

    def get_success_url(self):
        return reverse_lazy('reviews', kwargs={'pk': AuctionProduct.objects.get(pk=self.kwargs.get('pk')).seller.pk})

    def get(self, request, *args, **kwargs):
        user = self.request.user.bidder
        rating = Rating.objects.filter(auction__seller=user)
        if rating.exists():
            messages.add_message(self.request, INFO, 'Already sent feedback')
            return HttpResponseRedirect(reverse_lazy('history'))
        return super(UserReviewCreate, self).get(request, *args, **kwargs)


class UserReviewList(LoginRequiredMixin, ListView):
    model = Rating
    paginate_by = 15
    template_name = 'ratings/rating_listview.html'

    def get_queryset(self):
        user_id = self.kwargs.get('pk', None)
        return Rating.objects.filter(auction__seller_id=user_id)


class UserReviewUpdate(LoginRequiredMixin, UpdateView):
    model = Rating
    form_class = ReviewCreationForm
    template_name = 'ratings/rating_update.html'
    success_url = reverse_lazy('history')

    def get_object(self, queryset=None):
        bidder = self.request.user.bidder
        rating = get_object_or_404(Rating, pk=self.kwargs.get('pk'))
        if rating.bidder_rating != bidder:
            raise PermissionDenied
        return super(UserReviewUpdate, self).get_object(queryset=queryset)


class AuctionChat(LoginRequiredMixin, FormMixin, DetailView):
    model = AuctionProduct
    form_class = ChatMessage
    template_name = 'auction/auction_chat.html'

    def get_object(self, queryset=None):
        obj = super(AuctionChat, self).get_object(queryset)
        user = self.request.user
        if obj.has_started or obj.expired:
            messages.add_message(self.request, messages.WARNING, 'Chat can be used only before the auction starts')
        if obj and (obj.seller == user.bidder or user.is_staff):
            return obj
        raise PermissionDenied()


class AuctionChatList(LoginRequiredMixin, SearchFormView):
    model = AuctionProduct
    paginate_by = 10
    template_name = 'auction/auction_chat_listview.html'
    ordering = ('-date_end', 'name')
    form_class = SearchForm

    def get_queryset(self):
        if self.request.user.is_staff:
            query = Q(pending=True)
            return super(AuctionChatList, self).get_queryset().filter(query).distinct()
        raise PermissionDenied()

    def form_valid(self, form):
        name = form.cleaned_data.get('name')
        return AuctionProduct.objects.filter(name__icontains=name)


class AuctionApprove(LoginRequiredMixin, UpdateView):
    model = AuctionProduct
    fields = ['pending']
    template_name = 'auction/auction_approve.html'
    success_url = reverse_lazy('auction-chat-list')

    def get_queryset(self):
        if not self.request.user.is_staff:
            raise PermissionDenied()
        return super(AuctionApprove, self).get_queryset()

    def get_object(self, queryset=None):
        obj = super(AuctionApprove, self).get_object(queryset)
        if obj.has_started or obj.expired:
            messages.add_message(self.request, messages.WARNING, 'This auction cannot be approved')
        return obj

    def post(self, request, *args, **kwargs):
        data = super(AuctionApprove, self).post(request, *args, **kwargs)
        if not self.object.pending:
            messages.add_message(self.request, messages.SUCCESS, f'{self.object.name} has been approved')
        return data
