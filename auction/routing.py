from django.urls import path

from .consumers import JsonAuctionConsumer, JsonChatConsumer

ws_urlpatterns = [
    path('ws/auction-channel/<int:room_name>/', JsonAuctionConsumer.as_asgi()),
    path('ws/chat-channel/<int:room_name>/', JsonChatConsumer.as_asgi()),
]
