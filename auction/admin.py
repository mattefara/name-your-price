from django.contrib import admin
from django.utils import timezone

from .models import AuctionProduct, Bidder, Bid, Rating


class PendingListFilter(admin.SimpleListFilter):
    title = 'pending'
    parameter_name = 'pending'

    def lookups(self, request, model_admin):
        return (
            ('pending_true', 'Is pending'),
            ('pending_false', 'Approved'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'pending_true':
            return queryset.filter(pending=True)
        if self.value() == 'pending_false':
            return queryset.filter(pending=False)


class StartedListFilter(admin.SimpleListFilter):
    title = 'Auction state'
    parameter_name = 'date'

    def lookups(self, request, model_admin):
        return (
            ('not_started', 'Has not started'),
            ('expired', 'Is expired'),
            ('progress', 'In progress')
        )

    def queryset(self, request, queryset):
        now = timezone.now()
        if self.value() == 'progress':
            return queryset.filter(date_start__lte=now, date_end__gte=now)
        if self.value() == 'not_started':
            return queryset.filter(date_start__gte=now)
        if self.value() == 'expired':
            return queryset.filter(date_end__lte=now)


class BidInline(admin.TabularInline):
    model = Bid
    extra = 0


class AuctionModelAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)
    fieldsets = (
        (None, {'fields': ('name',)}),
        ('Request', {'fields': ('seller', 'pending')}),
        ('Dates', {'fields': ('date_start', 'date_end')}),
        ('Images', {'fields': ('product_image',)}),
        ('Category', {'fields': ('category', 'description')}),
    )
    list_filter = (PendingListFilter, StartedListFilter)
    inlines = (BidInline, )


class BidderModelAdmin(admin.ModelAdmin):
    search_fields = ('user__username',)


admin.site.register(AuctionProduct, AuctionModelAdmin)
admin.site.register(Bidder, BidderModelAdmin )
admin.site.register(Bid)
admin.site.register(Rating)
