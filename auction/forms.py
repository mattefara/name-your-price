from django import forms
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.text import gettext_lazy as _

from .models import Bid, AuctionProduct, Rating, Message


class BiddingForm(forms.ModelForm):
    amount = forms.IntegerField(min_value=1, required=True)
    auto_bid = forms.BooleanField(initial=True, required=False)
    auto_bid_amount = forms.IntegerField(required=False)
    step = forms.IntegerField(initial=1, min_value=1, required=False)

    def clean(self):
        cleaned_data = super(BiddingForm, self).clean()
        auto_bid = cleaned_data.get('auto_bid')
        auto_bid_amount = cleaned_data.get('auto_bid_amount')
        amount = cleaned_data.get('amount')
        step = cleaned_data.get('step')
        if not step:
            step = 1

        if auto_bid and not auto_bid_amount:
            raise ValidationError('Cannot send an auto bid amount without a max bid')
        if not auto_bid and auto_bid_amount:
            raise ValidationError('Cannot send a bid with a max amount without auto bid enabled')
        if auto_bid and amount + step > auto_bid_amount:
            raise ValidationError('Cannot send a bid with a max bid lower than the bid amount')

        return cleaned_data

    class Meta:
        model = Bid
        fields = ('amount',)


class SearchBiddingForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Search'}),
        label='',
        initial='',
        required=False
    )
    sold = forms.BooleanField(required=False, initial=False)
    all_categories = forms.BooleanField(initial=True, required=False)
    category = forms.TypedChoiceField(required=False, choices=AuctionProduct.AuctionCategory.choices)
    price = forms.IntegerField(required=False, initial=0, label='Price grater than', min_value=0)
    advanced = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)

    class Meta:
        model = AuctionProduct
        fields = ('name', 'category', 'sold', 'price')


class CreateAuctionProductForm(forms.ModelForm):
    date_start = forms.DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'value': (timezone.now() + timezone.timedelta(days=1)).strftime('%Y-%m-%d %H:%M')},
            format='%Y-%m-%dT%H:%M')
    )
    date_end = forms.DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'value': (timezone.now() + timezone.timedelta(days=2)).strftime('%Y-%m-%d %H:%M')},
            format='%Y-%m-%dT%H:%M')
    )

    def clean(self):
        cleaned_data = super(CreateAuctionProductForm, self).clean()
        start = cleaned_data.get('date_start')
        end = cleaned_data.get('date_end')
        if end < start:
            raise ValidationError('Date start cannot be grater than the start end')
        return cleaned_data

    class Meta:
        model = AuctionProduct
        fields = ('name', 'category', 'description', 'date_start', 'date_end', 'product_image')


class AuctionOrderingForm(forms.ModelForm):
    CHOICES = (
        ('name', _('Name')),
        ('category', _('Category')),
        ('last_bidder', _('Last bidder')),
        ('expired', _('Expired')),
        ('pending', _('Pending')),
    )

    ordering = forms.TypedChoiceField(required=False, choices=CHOICES, empty_value='name')
    is_seller = forms.BooleanField(required=False, initial=False)
    descending = forms.BooleanField(initial=False, required=False)

    class Meta:
        model = AuctionProduct
        fields = ('ordering',)


class ReviewCreationForm(forms.ModelForm):
    vote = forms.IntegerField(min_value=0, max_value=5)

    class Meta:
        model = Rating
        fields = ('title', 'description', 'vote')


class ChatMessage(forms.ModelForm):
    text = forms.Textarea()

    class Meta:
        model = Message
        fields = ('text',)


class SearchForm(forms.ModelForm):
    name = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Search'}))

    class Meta:
        model = AuctionProduct
        fields = ('name',)
