from django import template

from ..models import Bid
register = template.Library()


@register.filter(name="latest_search", is_safe=True)
def latest_search(form, search=None):
    if not search:
        search = {}
    for field_name, field_value in search.items():
        form.fields[field_name].initial = field_value
    return form


@register.simple_tag()
def has_offered(user, auction):
    return Bid.objects.filter(user_bid=user, auction=auction).count() > 0


@register.simple_tag()
def auction_last_offer(user, auction):
    return Bid.objects.filter(user_bid=user, auction=auction).latest('date')

