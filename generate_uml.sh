#!/usr/bin/python

path='docs/uml'

python manage.py graph_models accounts -o $path/account_app_uml.png
python manage.py graph_models auction -o $path/auction_app_uml.png
python manage.py graph_models purchase -o $path/purchase_app_uml.png
python manage.py graph_models -a -g -o $path/name_your_price_uml.png
