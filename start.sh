
python manage.py makemigrations
python manage.py migrate

echo 'Adding random data...'
python manage.py shell < random_data.py

echo 'Creating super user'
python manage.py createsuperuser